# Discrete wavelet methods for time series analysis using python

Several python libraries implement discrete wavelet transforms. However, none of
them, or at least none that I know, is aimed at scientific use. This library aims
at filling this gap, in particular considering discrete wavelet transform as de-
scribed by Percival and Walden (2000).

This module started as translation of the wmtsa Matlab toolbox (see
http://www.atmos.washington.edu/~wmtsa/), so most naming conventions and most of
the code structure follows their choices. The code uses a mix of python and
cython for improved performance.

The code presently reflects my needs and preferences, but contributions from
others are welcome. The code has to some extent been tested, but bugs are to be
expected.

The file example.py shows a basic usage example. If more people will show interest
in this module, I will provide further examples.

To use the toolbox, just download the wmtsa directory and add it your python path.

On some machines, the toolbox does not work, reporting:
 "fatal error: numpy/arrayobject.h: No such file or directory"

Try to follow this suggestion (using the appropriate path for your installation):
http://stackoverflow.com/a/14657424